'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', function($scope, $http) {
  $scope.pokemones = [];
  $scope.pokemonesFiltrados = [];
  $scope.filterText = '';
  $scope.pokemonSeleccionado = null;

  $scope.obtenerPokemones = function() {
      $http.get('https://pokeapi.co/api/v2/pokemon')
          .then(function(response) {
              $scope.pokemones = response.data.results;
              $scope.filtrarPokemones(); 
          });
  };

  $scope.filtrarPokemones = function() {
      $scope.pokemonesFiltrados = $scope.pokemones.filter(function(pokemon) {
          return !$scope.filterText || pokemon.name.toLowerCase().includes($scope.filterText.toLowerCase());
      });
  };

  $scope.mostrarDetalles = function(event, pokemon) {
    event.preventDefault();
    $http.get(pokemon.url)
        .then(function(response) {
            $scope.pokemonSeleccionado = response.data;
            var pokemonDetailsElement = document.getElementById("pokemon-details");
            pokemonDetailsElement.scrollIntoView({ behavior: "smooth" });
        }).catch(function(error) {
            console.error('Error al obtener los detalles del Pokémon:', error);
        });
};
$scope.imageUrls = [];

fetch('https://pokeapi.co/api/v2/pokemon-form/20/')
  .then(response => response.json())
  .then(data => {
    if (data.sprites.back_default) {
      $scope.imageUrls.push(data.sprites.back_default);
    }
    if (data.sprites.back_female) {
      $scope.imageUrls.push(data.sprites.back_female);
    }

    console.log('URLs de las imágenes:', $scope.imageUrls);
  })
  .catch(error => {
    console.error('Error al obtener las imágenes del Pokémon:', error);
  });
  $scope.obtenerPokemones();

  $http.get('https://pokeapi.co/api/v2/pokemon')
  .then(function(response) {
    var pokemones = response.data.results;
    $scope.resumenPokemones = {};
    pokemones.forEach(function(pokemon) {
      var primeraLetra = pokemon.name.charAt(0).toUpperCase();
      $scope.resumenPokemones[primeraLetra] = ($scope.resumenPokemones[primeraLetra] || 0) + 1;
    });

    $scope.letrasOrdenadas = Object.keys($scope.resumenPokemones).sort();
  })
  .catch(function(error) {
    console.error('Error al obtener la lista de Pokémones:', error);
  });
  $scope.cerrarDetalles = function() {
    $scope.pokemonSeleccionado = null; 
};



});